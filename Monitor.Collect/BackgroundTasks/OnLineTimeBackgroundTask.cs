﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monitor.Core;
using Monitor.Domain.Cluster.Dal;
using Monitor.Domain.Cluster.Model;
using Monitor.Domain.PlatformManage.Dal;
using BSF.BaseService.Monitor.SystemRuntime;
using BSF.Db;


namespace Monitor.Collect.BackgroundTasks
{
    public class OnLineTimeBackgroundTask : BaseBackgroundTask
    {

        public override void Start()
        {
            this.TimeSleep = GlobalConfig.OnLineTimeBackgroundTaskSleepTime * 1000;
            base.Start();
        }

        protected override void Run()
        {
            SqlHelper.ExcuteSql(DbShardingHelper.GetDataBase(GlobalConfig.DataBaseConfigModels, DataBaseType.PlatformManage), (c) =>
            {
                tb_cluster_dal dal = new tb_cluster_dal();
                dal.UpdateOnLineTime(c,GlobalConfig.ServerIP);
            });
        }
    }
}
